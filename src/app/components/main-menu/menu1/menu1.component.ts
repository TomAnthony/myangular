import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-menu1',
  templateUrl: './menu1.component.html',
  styleUrls: ['./menu1.component.scss']
})
export class Menu1Component implements OnInit {

  name: string;
  link1 = 'home';
  link2 = 'à propos';

  constructor() { }

  ngOnInit(): void {
    this.name = 'Jean-Michel';
  }

  user() {
    return {age: 32, name: 'Joe'}
  }

}
